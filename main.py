import tornado.websocket
import tornado.web
import tornado.ioloop
import json

PDU_REGISTER = 1
PDU_INPUT_FORWARD = 2
PUD_INPUT_SEND = 3

ws_obj = {}

class WebSocketHandler(tornado.websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True
    
    def on_message(self, message):
        msg = json.loads(message)

        pdu = msg.get("pdu", "")

        if pdu == PDU_REGISTER:
            uuid = msg.get("uuid", "")
            ws_obj[uuid] = self
        
        elif pdu == PDU_INPUT_FORWARD:
            value = msg.get("value", "")
            for uuid, client in ws_obj.items():
                if client != self:
                    client.write_message(json.dumps(dict(pdu=PDU_INPUT_FORWARD, value=value)))

        elif pdu == PUD_INPUT_SEND:
            value = msg.get("value", "")
            sender = msg.get("uuid")
            for uuid, client in ws_obj.items():
                client.write_message(json.dumps(dict(pdu=PUD_INPUT_SEND, value=value, uuid=sender)))
        
    def on_close(self):
        for uuid, client in ws_obj.items():
            if client == self:
                del ws_obj[uuid]


class MyApplication(tornado.web.Application):
    def __init__(self, **kwargs):
        kwargs["debug"] = True
        kwargs["autoreload"] = False
        kwargs["serve_traceback"] = True
        kwargs["handlers"] = [
            (r"/ws", WebSocketHandler),
        ]

        super(MyApplication, self).__init__(**kwargs)

def start():
    application = MyApplication()
    sockets = tornado.netutil.bind_sockets(8080)
    tornado.process.fork_processes(1)
 
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.add_sockets(sockets)

    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    start()
